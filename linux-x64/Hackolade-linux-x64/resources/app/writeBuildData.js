'use strict';

const fs = require('fs');
const path = require('path');

function writeBuildData(plat, arch) {
  let os, architecture;

  switch (plat) {
    case 'darwin':
      os = 'macintosh';
      break;
    case 'linux':
      os = 'linux';
      break;
    default:
      os = 'windows';
  }

  switch (arch) {
    case 'ia32':
      architecture = 'x32';
      break;
    default:
      architecture = 'x64';
  }

  let dir = path.join('dist', 'buildData.js');
  let data = `
    /*\n* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.\n
    *\n* The copyright to the computer software herein is the property of IntegrIT S.A.\n
    * The software may be used and/or copied only with the written permission of\n
    * IntegrIT S.A. or in accordance with the terms and conditions stipulated in\n
    * the agreement/contract under which the software has been supplied.\n
    */\n

    window.buildData = {\n
      \tos: '${os}',\n
      \tarchitecture: '${architecture}'\n
    };
  `;

  fs.writeFileSync(dir, data);

  if(plat === 'win32' || plat === 'linux') {
    let text = fs.readFileSync('package.json');
    let appVersion = JSON.parse(text).version;
    let versionWithComma = appVersion.replace(/\./g, ',');
    let vesionInfo = `
1 VERSIONINFO
FILEVERSION ${versionWithComma},0
PRODUCTVERSION ${versionWithComma},0
FILEOS 0x40004
FILETYPE 0x1
{
BLOCK "StringFileInfo"
{
	BLOCK "040904B0"
	{
		VALUE "CompanyName", "IntegrIT SA/NV."
		VALUE "FileDescription", "Hackolade"
		VALUE "FileVersion", "${appVersion}"
		VALUE "InternalName", "hackolade.exe"
		VALUE "LegalCopyright", "Copyright (C) 2016-2018 IntegrIT SA/NV. All rights reserved."
		VALUE "OriginalFilename", "hackolade.exe"
		VALUE "ProductName", "Hackolade"
		VALUE "ProductVersion", "${appVersion}"
		VALUE "SquirrelAwareVersion", "1"
	}
}

BLOCK "VarFileInfo"
{
	VALUE "Translation", 0x0409 0x04B0  
}
}`;
  let dirToVersion = path.join('dist', 'version.rc')
  fs.writeFileSync(dirToVersion, vesionInfo);
  }
}

module.exports = writeBuildData;