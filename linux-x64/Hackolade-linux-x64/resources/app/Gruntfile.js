module.exports = function(grunt) {
  grunt.initConfig({
    'create-windows-installer': {
      ia32: {
        appDirectory: './release/win32-ia32/Hackolade-win32-ia32',
        outputDirectory: './dist',
        name: 'Hackolade',
        description: 'Hackolade is an application for visual modeling of MongoDB schemas',
        authors: 'Hackolade',
        exe: 'Hackolade.exe',
        noMsi: false
      },
      x64: {
        appDirectory: './release/win32-x64/Hackolade-win32-x64',
        outputDirectory: './dist',
        name: 'Hackolade',
        loadingGif: './load.gif',
        title: 'Hackolade',
        setupIcon: 'icon.ico',
        description: 'Application for visual modeling of MongoDB schemas',
        authors: 'Hackolade',
        exe: 'Hackolade.exe',
        noMsi: false
      }
    }
  });

  grunt.loadNpmTasks('grunt-electron-installer');
};