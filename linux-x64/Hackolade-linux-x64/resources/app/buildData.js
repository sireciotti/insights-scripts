/*\n* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.\n
*\n* The copyright to the computer software herein is the property of IntegrIT S.A.\n
* The software may be used and/or copied only with the written permission of\n
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in\n
* the agreement/contract under which the software has been supplied.\n
*/

window.buildData = {
	os: 'macintosh',
	architecture: 'x64'
};