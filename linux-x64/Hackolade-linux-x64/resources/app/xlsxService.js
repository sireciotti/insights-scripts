
/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/

"use strict";var XLSX=require("xlsx");module.exports={generateHackoladeModelFromXlsx:function(e,o){var r=XLSX.readFile(e.filePath);o(this.workBookToJson(r))},workBookToJson:function(e){var o={};return e.SheetNames.forEach(function(r){var t=XLSX.utils.sheet_to_json(e.Sheets[r]);t.length>0&&(o[r]=t)}),o}};