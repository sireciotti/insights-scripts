# Privacy Policy #

## Our Commitment to Privacy ##

Your privacy is important to us.  We treat all personal information with extreme care and security.  Specifically, we do not sell, rent, or trade email lists with other companies for marketing purposes.  This policy also explains your choices about how we use information about you. Your choices include how you can object to certain uses of information about you and how you can access and update certain information about you. 

1. [Who is responsible for your data](responsible)
2. [What information we collect about you](what)
3. [How we use information we collect](use)
4. [How we share information we collect](share)
5. [How we store and secure information we collect](store)
6. [How to access and control your information](access)
7. [How long do we retain information we collect](retention)
8. [How we transfer information we collect internationally](transfer)
9. [Contact information](contact)

## <a name="responsible"></a>1. Who is responsible for your data ##
IntegrIT SA/NV dba Hackolade is responsible for your data.  Our registered address is Zwaluwenlaan 16, 1650 Beersel, Belgium.  We are registered as a company in Belgium under company number BE 0477.231.387.  

When you place an order for any of our products, data is collected by our e-commerce reseller FastSpring, and FastSpring is the controller of that information.  You may consult FastSpring's Privacy Policy during the checkout process, or [here](https://fastspring.com/privacy/ "FastSpring Privacy Policy").

Some of the information collected by FastSpring is shared with us, as described below.   While your data may be processed by other third-party providers carefully chosen by us, we are the controller of the data which we collect from you, and as such we control the ways your personal data is collected and the purposes for which your personal data is used.  

## <a name="what"></a>2. What information we collect about you ##
We collect information about you when you provide it to us, and when other sources provide it to us, as further described below.

On some pages, you can order products, make requests, and register to receive materials. The types of personal information collected at these pages are:

- name
- company name
- address 
- email address
- phone number 

When you install our software and validate the software license key, we also collect:

- unique computer identifier
- IP address  

## <a name="use"></a>3. How we use information we collect ##
We use the information you provide about yourself when placing an order.  

We use return e-mail addresses to answer the e-mail we receive, and such addresses are not used for any other purpose.

You can register with our website if you would like to receive our newsletter as well as updates on our new products and services.  

Finally, we never use or share the personally identifiable information provided to us online in ways unrelated to the ones described in this Policy without also providing you an opportunity to opt out or otherwise prohibit such unrelated uses.

## <a name="share"></a>4. How we share information we collect ##
We use a variety of third-party vendors to carry out services like websites management and hosting, online product purchases, credit card processing, license key management, and email communications.  We only share your personal data as necessary, such as to complete a transaction or to provide a product or service you have requested or authorized and only with vendors or agents working on our behalf for the purposes described in this Policy. 

In this case, your personal information will be shared with these agents or contractors but only for the purpose of performing services on behalf and under instructions of Hackolade and in accordance with this Policy.  It is our legitimate interest to share information with these parties for these purposes, and in order to provide you with a quality service. 

We will not give, sell, rent or loan any personal information to any other third party. We may disclose such information to respond to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend against legal claims. We may also share such information if we believe it is necessary in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of our Terms and Conditions, or as otherwise required by law. 

## <a name="store"></a>5. How we store and secure information we collect ##
To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect from loss, misuse, unauthorized access, disclosure, alteration and destruction. We use standard safeguards such as firewalls, secure passwords, and strict security procedures.  Communications between our computers and our server uses SSL/TLS encryption to ensure that your data is secure in-flight. 

Unfortunately, no company or service can guarantee complete security.  Unauthorized entry or use, hardware or software failure, and other factors, may compromise the security of user information at any time.  In the event of a data breach, we will issue a breach notification to the affected parties, without undue delay, and no later than 72 hours after having become aware of it.

## <a name="access"></a>6. How to access and control your information ##
You can access all your personally identifiable information that we collect online and maintain by emailing support@hackolade.com.  To protect your privacy and security, we will take reasonable steps to verify your identity before granting access or making corrections.  We use this procedure to better safeguard your information.  You can correct factual errors in your personally identifiable information by sending us a request that credibly shows error.

## <a name="retention"></a>7. How long do we retain information we collect ##
We retain personal information as long as needed, to provide service to our customers, subject to our compliance with this policy.  We retain and use this personal information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. 

## <a name="contact"></a>8. How we transfer information we collect internationally ##
We collect information globally and primarily store that information in the United States.  We transfer, process and store your information outside of your country of residence, to wherever we, Hackolade, or our third-party service providers operate for the purpose of providing you the services.  Whenever we transfer your information, we take steps to protect it.

## <a name="contact"></a>9. Contact information ##
Should you have other questions or concerns about these privacy policies, please send us an e-mail at support@hackolade.com or contact us at IntegrIT SA/NV dba Hackolade, Zwaluwenlaan 16, 1650 Beersel, Belgium.
  