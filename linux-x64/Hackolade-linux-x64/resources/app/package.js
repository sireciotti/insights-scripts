/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in
* the agreement/contract under which the software has been supplied.
*/
/* eslint strict: 0, no-shadow: 0, no-unused-vars: 0, no-console: 0 */
'use strict';

const os = require('os');
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const async = require('async');
const cfg = require('./webpack.config.production.js');
const packager = require('electron-packager');
const del = require('del');
const exec = require('child_process').exec;
const argv = require('minimist')(process.argv.slice(2));
const pkg = require('./package.json');
const minifyService = require('./minifyService');
const mainService = require('./mainService');
const devDeps = Object.keys(pkg.devDependencies);

const appName = argv.name || argv.n || pkg.productName;
const shouldUseAsar = argv.asar || argv.a || false;
const shouldBuildAll = argv.all || false;
const shouldBuildLinuxOnly = argv.linux || false;
const shouldBuildWindowsOnly = argv.windows || false;
const shouldBuildDarwinOnly = argv.darwin || false;

const licenseData = `
/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/
`;

const writeBuildData = require('./writeBuildData');
const filesToMinify = ['mongoDBService', 'main', 'couchbaseService', 'dynamoDBService', 'generalDBService', 'xlsxService', 'terminalService',
                      'terminalConfig', 'documentationService', 'printService', 'mainService', 'eventHelper',
                      ['RE_API', 'probabilisticSchemaService'], ['RE_API', 'worker'], ['RE_API', 'apiWorker'], ['RE_API', 'revEngService'],
                      ['FE_API', 'apiWorker'], ['FE_API', 'forwardEngineeringService'],
                      ['log', 'licenseLogService'], ['log', 'revEngLogService'], ['log', 'generalLogService']];

const DEFAULT_OPTS = {
  dir: './',
  name: appName,
  asar: shouldUseAsar,
  ignore: [
    '/test($|/)',
    '/documentation($|/)',
    '/tools($|/)',
    `^/release($|/)`,
    '/app($|/)',
    '/custom_modules($|/)',
    '/node_modules/react-hot-loader($|/)',
    './main',
    './webpack.*',
    './README',
    './minifyService',
    './Gruntfile',
    './writeBuildData',
    '^/develop',
    '^/installation',
    '^/resources',
    '^/plugins',
    '/hackolade_scripts$'
  ].concat(devDeps.map(name => `/node_modules/${name}($|/)`))
};

const linuxPlatforms = [{
  platform: 'linux',
  arch: 'ia32'
},{
  platform: 'linux',
  arch: 'x64'
}];

const linuxPaths = [
  'linux-x64/Hackolade-linux-x64/resources/app',
  'linux-ia32/Hackolade-linux-ia32/resources/app'
];

const linuxCopies = [{
  src: "../release/linux-x64/Hackolade-linux-x64/resources/app/Hackolade Help.chm",
  dst: "../release/linux-x64/Hackolade-linux-x64/Hackolade Help.chm"
},{
  src: "../release/linux-x64/Hackolade-linux-x64/resources/app/License.txt",
  dst: "../release/linux-x64/Hackolade-linux-x64/License.txt"
},{
  src: "../release/linux-x64/Hackolade-linux-x64/resources/app/ReadMe.txt",
  dst: "../release/linux-x64/Hackolade-linux-x64/ReadMe.txt"
}];

const windowsPlatforms = [{
  platform: 'win32',
  arch: 'ia32'
},{
  platform: 'win32',
  arch: 'x64'
}];

const windowsPaths = [
  'win32-x64/Hackolade-win32-x64/resources/app',
  'win32-ia32/Hackolade-win32-ia32/resources/app'
];

const darwinPlatforms = [{
  platform: 'darwin',
  arch: 'x64'
}];

const darwinPaths = [
  'darwin-x64/Hackolade-darwin-x64/Hackolade.app/Contents/Resources/app'
];

const version = argv.version || argv.v;

if (version) {
  DEFAULT_OPTS.version = version;
  startPack();
} else {
  /*
  // use the same version as the currently-installed electron-prebuilt
  exec('npm list electron-prebuilt', (err, stdout) => {
    if (err) {
      DEFAULT_OPTS.version = '1.4.13';
    } else {
      DEFAULT_OPTS.version = stdout.split('electron-prebuilt@')[1].replace(/\s/g, '');
    }

    startPack();
  });
  */
  DEFAULT_OPTS.version = require('electron/package.json').version;
  startPack();
}


function startPack() {
  console.log('start pack...');
  var platforms = [];
  webpack(cfg, (err, stats) => {
    if (err) return console.error(err);
    del('release')
    .then(paths => {
      if (!shouldBuildAll && !shouldBuildLinuxOnly && !shouldBuildWindowsOnly && !shouldBuildDarwinOnly){
        const platform = {
          platform: os.platform(),
          arch: os.arch(),
        };
        pack(platform, log(os.platform(), os.arch()));
      } else {
        if (shouldBuildAll) {
          platforms = platforms.concat(linuxPlatforms).concat(darwinPlatforms).concat(windowsPlatforms);
        } else if (shouldBuildLinuxOnly){
          platforms = platforms.concat(linuxPlatforms);
        } else if (shouldBuildWindowsOnly){
          platforms = platforms.concat(windowsPlatforms);
        } else if (shouldBuildDarwinOnly){
          platforms = platforms.concat(darwinPlatforms);
        }

        async.mapSeries(platforms, pack, function(){
          minifyBackend();
        });
      }
    })
    .then(() => {
      return mainService.saveChecksumToFile();
    })
    .catch(err => {
      console.error(err);
    });
  });
}

function pack(platform, cb) {
  // there is no darwin ia32 electron
  const plat = platform.platform;
  const arch = platform.arch;

  const icon = argv.icon || argv.i || 'app/app.icns';

  const opts = Object.assign({}, DEFAULT_OPTS, {
    platform: plat,
    arch: arch,
    prune: false,
    out: `release/${plat}-${arch}`
  });

  if (plat !== 'win32'){
    opts.icon = icon;
  }

  writeBuildData(plat, arch);
  packager(opts, () => {
    log(plat, arch)();
    const args = [].slice.call(arguments);
    args.unshift(null);
    cb.apply(null, args);
  });
}

function minifyBackend(cb){
  var platformPaths = [];

  if (shouldBuildAll) {
    platformPaths = platformPaths.concat(linuxPaths).concat(darwinPaths).concat(windowsPaths);
  } else if (shouldBuildLinuxOnly){
    platformPaths = platformPaths.concat(linuxPaths);
  } else if (shouldBuildWindowsOnly){
    platformPaths = platformPaths.concat(windowsPaths);
  } else if (shouldBuildDarwinOnly){
    platformPaths = platformPaths.concat(darwinPaths);
  }

  async.map(filesToMinify, function(fileName, cb){
    minifyService(fileName, licenseData, cb);
  }, function(){
      async.map(platformPaths, writeMinifiedFiles, cb);
  });
}

function writeMinifiedFiles(item, callback){

  writeMinifiedFile(item, 0, callback);

  function writeMinifiedFile(pathItem, fileIndex, cb){
    var file = filesToMinify[fileIndex];

    if (file instanceof Array){
      file = file.reduce(function(a,b){
        if (!a){
          return a += b;
        }
        return a += '/' + b;
      }, '');
    }

    var stream = fs.createReadStream(`${__dirname}/${file}.min.js`).pipe(fs.createWriteStream(`${__dirname}/release/${pathItem}/${file}.js`));
    stream.on('close', () => {
      // fs.unlinkSync(`${__dirname}/${file}.min.js`);
      if (fileIndex < filesToMinify.length - 1) {
        writeMinifiedFile(pathItem, fileIndex + 1, cb);
      } else {
        callback();
      }
    });
  }

}

function log(plat, arch) {
  return (err, filepath) => {
    if (err) return console.error(err);
    console.log(`${plat}-${arch} finished!`);
  };
}
