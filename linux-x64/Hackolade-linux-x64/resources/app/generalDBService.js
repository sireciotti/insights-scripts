
/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/

"use strict";module.exports={getSampleDocSize:function(e,a){var t=void 0,l=a.maxValue||1e4;if("relative"===a.active){var i=a.relative.value;t=Math.ceil(e/100*i)}else{var r=a.absolute.value;t=r>e?e:r}return t=t>l?l:t}};