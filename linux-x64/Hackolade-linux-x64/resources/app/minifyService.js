var babel = require('babel-core');
var UglifyJS = require('uglify-js');
var fs = require('fs');

module.exports = function(fileName, licenseData, cb){

	if (fileName instanceof Array){
		fileName = fileName.reduce(function(a,b){
		if (!a){
			return a += b;
		}
		return a += '/' + b;
		}, '');
	}

	const fileIn = `./${fileName}.js`;
	const fileOut = `${fileName}.min.js`;
	var obj = babel.transformFileSync(fileIn);
	var result = UglifyJS.minify(obj.code, {
		fromString: true,
		output: {
			preamble: licenseData
		}
	});

	fs.writeFile(__dirname + '/' + fileOut, result.code, function(err){
		if (err) throw err;
		cb();
	});	
}