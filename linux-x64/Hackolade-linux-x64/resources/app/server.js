/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/
/* eslint strict: 0, no-console: 0 */
'use strict';

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const config = require('./webpack.config.development');

const app = express();
const compiler = webpack(config);

const PORT = 3000;

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  stats: {
    colors: true
  }
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/buildData.js', (req, res) => {
  res.sendFile(path.join(__dirname, 'buildData.js'));
});

app.get('/modules', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist', 'modules.bundle.js'));
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'app', 'hot-dev-app.html'));
});

app.listen(PORT, 'localhost', err => {
  if (err) {
    console.log(err);
    return;
  }

  console.log(`Listening at http://localhost:${PORT}`);
});