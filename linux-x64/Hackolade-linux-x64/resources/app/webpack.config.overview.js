/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/
/* eslint strict: 0 */
'use strict';

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const baseConfig = require('./webpack.config.base');
const path = require('path');

const config = Object.create(baseConfig);

config.entry = './develop/overview/overview';

config.output.publicPath = './develop/overview';
config.output.path = path.join(__dirname, 'develop', 'overview');
config.output.filename = 'overview.bundle.js';
config.output.libraryTarget = 'var';

config.module.rules.push({
  test: /^((?!\.module).)*\.css$/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: 'css-loader'
  })
}, {
  test: /\.module\.css$/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]'
  })
},
{
  test: /\.json$/,
  loader: 'json-loader',
}, {
  test: /\.txt$/,
  loader: 'raw-loader',
}, {
  test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
  loader: 'url-loader?limit=10000',
}, {
  test: /\.(eot|ttf|wav|mp3)$/,
  loader: 'url-loader',
});

config.resolve.alias = {
  'electron': '../../../develop/overview/electron-stub.js'
};

config.plugins.push(
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.DefinePlugin({
    '__DEV__': false,
    'process.env': {
      'NODE_ENV': JSON.stringify('production'),
      'BABEL_ENV': JSON.stringify('production')
    }
  }),
  new ExtractTextPlugin({
    filename: 'style.css',
    allChunks: true
  }),
  new webpack.optimize.ModuleConcatenationPlugin()
);

config.target = 'web';

module.exports = config;