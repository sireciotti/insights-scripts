/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/
/* eslint strict: 0 */
'use strict';

const webpack = require('webpack');
const baseConfig = require('./webpack.config.base');
const path = require('path');
//const DashboardPlugin = require('webpack-dashboard/plugin');

const config = Object.create(baseConfig);

config.devtool = 'source-map';

config.entry = [
	'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr',
	'./app/index'
];

config.output.publicPath = 'http://localhost:3000/dist/';
config.output.path = path.join(__dirname, 'dist');

config.module.rules.push({
	test: /^((?!\.module).)*\.css$/,
	use: [{
		loader: 'style-loader'
	},{
		loader: 'css-loader'
	}]
}, {
	test: /\.module\.css$/,
	use: [{
		loader: 'style-loader'
	},{
		loader: 'css-loader'
	}]
},
{
	test: /\.json$/,
	loader: 'json-loader',
}, {
	test: /\.txt$/,
	loader: 'raw-loader',
}, {
	test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
	loader: 'url-loader?limit=10000',
}, {
	test: /\.(eot|ttf|wav|mp3)$/,
	loader: 'file-loader',
});


config.plugins.push(
	new webpack.HotModuleReplacementPlugin(),
	new webpack.NoEmitOnErrorsPlugin(),
	new webpack.DefinePlugin({
		'__DEV__': true,
		'process.env': {
			'NODE_ENV': JSON.stringify('development')
		}
	}),
	new webpack.LoaderOptionsPlugin({
		debug: true,
		options: {
			context: __dirname
		}
	})
    //new DashboardPlugin()

);

module.exports = config;