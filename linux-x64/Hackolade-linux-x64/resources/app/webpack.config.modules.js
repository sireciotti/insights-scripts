/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/
/* eslint strict: 0 */
'use strict';

const webpack = require('webpack');
const path = require('path');

const fs = require('fs');
const _ = require('lodash');

const packageJson = JSON.parse(fs.readFileSync(__dirname + '/package.json'));
let packages = Object.keys(packageJson.dependencies);

packages.push('./custom_modules/split.js', './custom_modules/react-tree-menu/index.js', './custom_modules/jayschema/lib/jayschema.js')
packages = _.difference(packages, ['spellchecker', 'kerberos', 'xlsx', 'font-awesome', 'cross-env']);


const config = {
	entry: {
		'modules': packages
	},

	module: {
		rules: [{
			test: /^((?!\.module).)*\.css$/,
			use: [{
				loader: 'style-loader'
			},{
				loader: 'css-loader'
			}]
		}, {
			test: /\.module\.css$/,
			use: [{
				loader: 'style-loader'
			},{
				loader: 'css-loader'
			}]
		}, {
			test: /\.json$/,
			loader: 'json-loader',
		}, {
			test: /\.txt$/,
			loader: 'raw-loader',
		}, {
			test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/,
			loader: 'url-loader?limit=10000',
		}, {
			test: /\.(eot|ttf|wav|mp3)$/,
			loader: 'file-loader',
		}, {
			test: /\.jsx?$/,
			loader: 'babel-loader',
			exclude: [/node_modules/, /custom_modules/]
		}, 
		{
			test: /\.scss$/,
			use: [{
				loader: 'style-loader'
			}, {
				loader: 'css-loader'
			}, {
				loader: 'sass-loader'
			}]
		}]
	},

	output : {
		filename: '[name].bundle.js',
		path: 'dist/',
		library: '[name]_lib',
	},

	plugins: [
		new webpack.DllPlugin({
			path: path.join(__dirname, "dist", "[name]-manifest.json"), 
			name: "[name]_lib"
		}),
		new webpack.IgnorePlugin(/vertx/)
	]
};

module.exports = config;