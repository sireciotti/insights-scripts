
/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/

"use strict";var path=require("path"),logService=require(path.resolve(__dirname,"../log/revEngLogService")),logger=logService;process.on("message",function(e){var r=e.eventType;try{var n=require(e.pluginPath+"/reverse_engineering/api"),s=require(e.pluginPath+"/package.json");n[r](e.arg,logger,function(){var e=arguments.length>0&&void 0!==arguments[0]?arguments[0]:null,n=arguments.length>1&&void 0!==arguments[1]?arguments[1]:null,o=arguments.length>2&&void 0!==arguments[2]?arguments[2]:null,t=arguments.length>3&&void 0!==arguments[3]?arguments[3]:null;process.send({err:e,result:n,info:o,eventType:r,relationships:t,packageConfig:s})})}catch(e){process.send({err:e.stack,result:null,eventType:r})}}),process.on("uncaughtException",function(e){process.send({err:e.stack,result:null})}),process.on("disconnect",function(){process.exit(0)});