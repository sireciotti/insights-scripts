
/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/

"use strict";function transformData(r){try{var e=Buffer.concat(r),n=[],s=chunkDecoder(e,"##_hackolade_##_chunk"),a=s();if(-1===a)return{err:"Buffer is empty"};do{n=n.concat(a.docs),a=s()}while(-1!==a);return{data:n}}catch(r){return{err:r.stack}}}var parseSchema=require("mongodb-schema"),BSON=require("bson"),fs=require("fs"),serializedDataChunks=[],data=[];process.stdin.on("data",function(r){serializedDataChunks.push(r)}),process.stdin.on("end",function(){try{var r=transformData(serializedDataChunks),e=r.data,n=r.err;if(n)return process.send({err:n,probabilisticSchema:null});parseSchema(e,function(r,e){r?process.send({err:r.stack,probabilisticSchema:null}):(e=JSON.stringify(e,null,4),e=JSON.parse(e),process.send({err:r,probabilisticSchema:e}))})}catch(n){process.send({err:n.stack,probabilisticSchema:null})}}),process.on("uncaughtException",function(r){process.send({err:r.stack,probabilisticSchema:null})}),process.on("disconnect",function(){process.exit(0)});var chunkDecoder=function(r,e){var n=0,s=r.indexOf(e,n),a=Buffer.from(e).length;return function(){if(s>0){var c=r.slice(n,s),t=new BSON,i=t.deserialize(c);return n=s+a,s=r.indexOf(e,n),i}return-1}};