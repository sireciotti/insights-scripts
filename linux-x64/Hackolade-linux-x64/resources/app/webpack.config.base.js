/*
* Copyright © 2016-2018 by IntegrIT S.A. dba Hackolade.  All rights reserved.
*
* The copyright to the computer software herein is the property of IntegrIT S.A.
* The software may be used and/or copied only with the written permission of 
* IntegrIT S.A. or in accordance with the terms and conditions stipulated in 
* the agreement/contract under which the software has been supplied. 
*/
/* eslint strict: 0 */
'use strict';

const path = require('path');
const webpack = require('webpack');

module.exports = {
	target: 'electron-renderer',
	module: {
		rules: [{
			test: /\.jsx?$/,
			loader: 'babel-loader',
			exclude: [/node_modules/, /custom_modules/]
		}, 
		{
			test: /\.scss$/,
			use: [{
				loader: 'style-loader'
			}, {
				loader: 'css-loader?-autoprefixer'
			}, {
				loader: 'sass-loader'
			}]
		}],
		noParse: [/html2canvas/]
	},
	output: {
		filename: 'bundle.js',
		libraryTarget: 'commonjs2',
		pathinfo: true
	},
	resolve: {
		enforceExtension: false,
		extensions: ['.js', '.node','.jsx'],
		mainFields: ['webpack', 'browser', 'web', 'browserify', ['jam'], 'main'],
		modules: ['node_modules', 'custom_modules']
	},
	plugins: [
		new webpack.IgnorePlugin(/vertx/),
		new webpack.LoaderOptionsPlugin({
			minimize: true
		})
	]
};