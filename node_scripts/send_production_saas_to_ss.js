var MongoClient = require('mongodb').MongoClient;
const url = "mongodb://devserver.hq.eadbox.com:27017/";
const request = require("request");

MongoClient.connect(url, { useNewUrlParser: true }, function(err, db){
  var db_manage = db.db("eadbox_production");
  var collection = db_manage.collection("saas");
  var cursor = collection.find({blocked:false});
  cursor.project({
    _id: 1,
    blocked: 1,
    certificate: 1,
    courses_at_courses: 1,
    courses_at_home: 1,
    currency_unit: 1,
    description: 1,
    email: 1,
    facebook_user_id: 1,
    google_analytics_account_id: 1,
    google_tag_manager_id: 1,
    live_streaming: 1,
    name: 1,
    owner_id: 1,
    payment_method: 1,
    ranking: 1,
    s3_video_formats: 1,
    slug: 1,
    template: 1,
    title: 1,
    integrations: 1,
    enforce_angular: 1,
    session_limit: 1,
    enforce_ssl: 1,
    encrypted_s3_video_formats: 1,
    enable_score_ranking: 1,
    enable_collectors_ranking: 1,
    enable_last_badges_ranking: 1,
    enforce_angular_teacher: 1,
    show_angular_teacher: 1,
    new_landing_page: 1
  })
  .toArray(function(err, result) {
    if (err){
      throw err;
    } else {
      var options = {
        "method": "POST",
        "url": "https://script.google.com/macros/s/AKfycby4quBiVJYG3IF7b05yB-DyO5XoirflvMevvmlB3bDPK1e_dkjE/exec",
        "headers": {
          'Content-Type': 'application/json'
        },
        "body": JSON.stringify({
          'ss': 'saas-prod',
          'data': JSON.stringify(result)
        })
      };
      request(options, function (error, response, body) {
    		if (error) {
    			throw error;
    		} else {
          console.log(body);
        }
      });
    }
    db.close();
  });
});
