var MongoClient = require('mongodb').MongoClient;
const url = "mongodb://devserver.hq.eadbox.com:27017/";
const request = require("request");

MongoClient.connect(url, { useNewUrlParser: true }, function(err, db){
  var db_manage = db.db("eadbox_production");
  var collection = db_manage.collection("subscriptions");
  var cursor = collection.aggregate(
    [
      {
        $lookup: {
          from: 'saas',
          localField: 'saas_id',
          foreignField: '_id',
          as: 'saas'
        }
      },
      {
        $unwind: {
          path: '$saas',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $match: {
          $and: [
            { 'saas.blocked': false },
            { finished_percentage: { $gte: 0 } }
          ]
        }
      }
    ]
  );

  cursor.project({
    _id:1,
    finished_percentage:1,
    saas_id: 1,
    course_id: 1
  })
  .toArray(function(err, result) {
    if (err){
      throw err;
    } else {
      var options = {
        "method": "POST",
        "url": "https://script.google.com/macros/s/AKfycby4quBiVJYG3IF7b05yB-DyO5XoirflvMevvmlB3bDPK1e_dkjE/exec",
        "headers": {
          'Content-Type': 'application/json'
        },
        "body": JSON.stringify({
          'ss': 'subscriptions-prod',
          'data': JSON.stringify(result)
        })
      };
      request(options, function (error, response, body) {
    		if (error) {
    			throw error;
    		} else {
          console.log(body);
        }
      });
    }
    db.close();
  });
});
