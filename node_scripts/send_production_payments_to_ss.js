var MongoClient = require('mongodb').MongoClient;
const url = "mongodb://devserver.hq.eadbox.com:27017/";
const request = require("request");

MongoClient.connect(url, { useNewUrlParser: true }, function(err, db){
  var db_manage = db.db("eadbox_production");
  var collection = db_manage.collection("payments");
  var cursor = collection.aggregate(
    [
      {
        $lookup: {
          from: 'saas',
          localField: 'saas_id',
          foreignField: '_id',
          as: 'saas'
        }
      },
      {
        $unwind: {
          path: '$saas',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $match: {
          $and: [
          { 'saas.blocked': false },
          { created_at: {$gte: new Date(new Date() - 1000 * 3600 * 24 * 30)}},
          {
            payment_method: {
              $in: [
                'PaymentMethod::PagamentoDigital',
                'PaymentMethod::Paypal',
                'PaymentMethod::EadboxPayment',
                'PaymentMethod::SuperLogica',
                'PaymentMethod::Stripe',
                'PaymentMethod::MercadoPago',
                'PaymentMethod::Pagarme'
              ]
            }
          }
          ]
        }
      }
    ]
  );

  cursor.project({
    saas_id: 1,
    subscription_id: 1,
    payment_method: 1,
    payment_status: 1,
    value: 1
  })
  .toArray(function(err, result) {
    if (err){
      throw err;
    } else {
      var options = {
        "method": "POST",
        "url": "https://script.google.com/macros/s/AKfycby4quBiVJYG3IF7b05yB-DyO5XoirflvMevvmlB3bDPK1e_dkjE/exec",
        "headers": {
          'Content-Type': 'application/json'
        },
        "body": JSON.stringify({
          'ss': 'payments-prod',
          'data': JSON.stringify(result)
        })
      };
      request(options, function (error, response, body) {
    		if (error) {
    			throw error;
    		} else {
          console.log(body);
        }
      });
    }
    db.close();
  });
});
