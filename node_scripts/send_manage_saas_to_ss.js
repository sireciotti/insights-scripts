var MongoClient = require('mongodb').MongoClient;
const url = "mongodb://devserver.hq.eadbox.com:27017/";
const request = require("request");

MongoClient.connect(url, { useNewUrlParser: true }, function(err, db){
  var db_manage = db.db("eadbox_manager_production");
  var saasCollection = db_manage.collection("saas");
  var aggregation = saasCollection.aggregate([
      {
        $lookup: {
          from: 'plans',
          localField: 'plan_id',
          foreignField: '_id',
          as: 'plans'
        }
      },
      {
        $unwind: {
          path: '$plans',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: 'users',
          localField: 'owner_id',
          foreignField: '_id',
          as: 'users'
        }
      },
      {
        $unwind: {
          path: '$users',
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $match: {
          $and: [
            { blocked_at: null },
            { plan_id: { $ne: null } },
            { 'plans._slugs.0': {
                $nin: [
                  'teste-demo',
                  'interna',
                  'demostracion',
                  'demonstracion-mx',
                  'parceiros',
                  'partner-latam'
                ]
              } },
            { 'plans.name': { $nin: ['.', 'Recursos\ Humanos'] } },
            { 'users.email': {$regex: /^(?!.*@eadbox).*$/, $options: 'ix'}}
          ]
        }
      },
      {
        $project: {
          'users':0,
          'plans':0
        }
      }
    ])
  .toArray(function(err, result) {
    if (err){
      throw err;
    } else {
      var options = {
        "method": "POST",
        "url": "https://script.google.com/macros/s/AKfycby4quBiVJYG3IF7b05yB-DyO5XoirflvMevvmlB3bDPK1e_dkjE/exec",
        "headers": {
          'Content-Type': 'application/json'
        },
        "body": JSON.stringify({
          'ss': 'saas-manage',
          'data': JSON.stringify(result)
        })
      };
      request(options, function (error, response, body) {
    		if (error) {
    			throw error;
    		} else {
          console.log(body);
        }
      });
    }
    db.close();
  });
});
